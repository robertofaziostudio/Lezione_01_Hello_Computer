# IED FIRENZE - INTERACTION & MULTIMEDIA DESIGN #
Corso di Creative Coding con openFrameworks IED Firenze 2017-2018

### LEZIONE 01 ###

#### Introduzione generale ####

* Aesthetics + Computation : Pensiero computazionale, approcci, paradigmi e case history
* Overview progetti di generative design, creative coding e interaction design
* Come pensa e ragiona un computer: codice decimale, binario, esa-decimale
* I programmi: dall’ideazione all’esecuzione ( codice sorgente, compilatore, assembler, eseguibile )
* Cenni storici sui linguaggi di programmazione in particolare al C++

#### Hello Computer - Introduzione al C++ ####

* Hello Computer : creare un vero programma
* Struttura base di un programma C++ ( http://ideone.com )
* Creare un programma C++ con linea di comando. Introduzione a CLI ( command line tool ) http://openframeworks.cc/ofBook/chapters/cplusplus_basics.html
* Creare un programma C++ con un IDE ( Integrated development environment ): Xcode per OS X 

[ Donwload Lezione 01 Keynote : ](Resources/01_CreativeCoding.key)


Docente : Roberto Fazio 
www.robertofazio.com
studio@robertofazio.com